<?php
/**
 * @file
 * Alternate formatters for some fields.
 */

/**
 * Implements hook_field_formatter_info().
 *
 * Declares the existence of this formatter.
 * We can do similar things to local textareas, remote URLs, or uploaded files!
 */
function advanced_formatters_field_formatter_info() {
  return array(
    'raw' => array(
      'label' => t('Displayed raw'),
      'field types' => array(
        'text_long',
        'file',
        'link_field'
      ),
      'settings' => array(
        'debug' => FALSE,
      ),
    ),
    'json_table' => array(
      'label' => t('JSON as Table'),
      'field types' => array(
        'text_long',
        'text_with_summary',
        'file',
        'link_field'
      ),
      'settings' => array(
        'debug' => FALSE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_summary().
 *
 * Summarizes the settings for display on the UI.
 */
function advanced_formatters_field_formatter_settings_summary($field, $instance, $view_mode) {
  // $display = $instance['display'][$view_mode];
  // $settings = $display['settings'];
  switch ($instance['display'][$view_mode]['type']) {
    case 'json_table':
      return t('Displayed formatted from JSON', array());
      break;

    case 'raw':
      return t('Displayed raw', array());
      break;
  }
}

/**
 * Implements hook_field_formatter_settings_form().
 *
 * Settings for the display options.
 */
function advanced_formatters_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element['debug'] = array(
    '#title' => t('Show warnings'),
    '#type' => 'checkbox',
    '#description' => t("If there was a problem, return the error message. Disable this for a public site."),
    '#default_value' => $settings['debug'],
  );
  return $element;
}


/**
 * Implements hook_field_formatter_view().
 *
 * Does the process here, to generate the result.
 * Delegates the final layout to the theme func
 */
function advanced_formatters_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  foreach ($items as $delta => $item) {

    $result = "Can't parse the input";
    $data = @$item['value'];

    // Alternate field types. Local data (values) is easiest, but
    // If the field type is a link, go get that data now
    // DESPERATELY need caching or something here.
    if ($field['type'] == 'link_field') {
      $url = url($item['url'], $item);
      $data = file_get_contents($url);
    }
    // Or files, why not?
    if ($field['type'] == 'file') {
      $data = file_get_contents($item['uri']);
    }

    try {
      $result = $data;

      // Now pretty-print it or cook it or not.
      switch ($display['type']) {
        case 'json_table':
          if (empty($result)) {
            continue;
          }
          $parsed = json_decode($result);
          if (!empty($parsed)) {
            // Render the json as a key-value struct
            $element[$delta] = array(
              '#theme' => 'advanced_formatters_struct',
              '#item' => $parsed,
              '#settings' => $display['settings'],
            );
          }
          else {
            // Result was not empty, but was not valid JSON.
            $element[$delta] = array(
              '#markup' => "JSON error <pre>$result</pre>",
            );
          }
          break;

        case 'raw':
          // noop
          $element[$delta] = array(
            '#theme' => 'advanced_formatters_raw',
            '#item' => $item,
            '#settings' => $display['settings'],
            '#result' => $result,
          );
          break;
      }
    } catch (Exception $e) {
      watchdog('advanced_formatters', "Unable to format data. %message", array('%message' => $e->getMessage()), WATCHDOG_ERROR);
    }

  }
  return $element;
}


/**
 * Implements hook_theme().
 *
 * Advertises our theme function.
 */
function advanced_formatters_theme() {
  return array(
    'advanced_formatters_raw' => array(
      'variables' => array(
        'item' => NULL,
        'settings' => NULL,
        'result' => NULL,
      ),
    ),
    'advanced_formatters_struct' => array(
      'variables' => array(
        'item' => NULL,
        'settings' => NULL,
      ),
    ),
  );
}

/**
 * Returns HTML presenting the field data raw.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An array of field data.
 *   - settings: used to do the transform,
 *   - result: the rendered result.
 *
 * @ingroup themeable
 */
function theme_advanced_formatters_raw($variables) {
  // The data is already cooked, just use this theme func to stick a
  // wrapper around it if you want.
  return $variables['result'];
}

/**
 * HTML-style data dumper.
 *
 * @param $data
 * @return string
 */
function theme_advanced_formatters_struct($variables) {
  // Normally called with a $variables['item'] array, but also can be called
  // directly with the data for the same effect.
  if (is_array($variables) && !empty($variables['item'])) {
    $data = $variables['item'];
  }
  else {
    $data = $variables;
  }
  if (empty($data)) {
    return NULL;
  }
  if (is_object($data)) {
    // Objects become tables.
    $rows = array();
    foreach ($data as $key => $val) {
      $safe_val = theme_advanced_formatters_struct($val);
      $rows[] = array(array('data' => $key, 'header' => TRUE), $safe_val);
    }
    return theme('table', array('rows' => $rows));
  }
  elseif (is_array($data)) {
    // Arrays become lists.
    $safe_vals = array_map('theme_advanced_formatters_struct', $data);
    return theme('item_list', array('items' => $safe_vals));
  }
  else {
    return $data;
  }
}
