<?php
/**
 * @file
 * Tests for the More Classes module
 */

/**
 * Test for More Classes admin UI.
 */
class MoreClassesUITestCase extends DrupalWebTestCase {

  private $adminUser;
  private $testBundle = 'article';
  private $testField = 'field_image';

  /**
   * @inheritdoc
   */
  public static function getInfo() {
    return array(
      'name' => 'More Classes',
      'description' => 'Tests using the edit UI to configure options.',
      'group' => 'Fantastic Semantic Markup',
    );
  }

  /**
   * @inheritdoc
   */
  public function setUp() {
    parent::setUp(array(
      'field_ui',
      'field_formatter_settings',
      'more_classes',
    ));

    // Seriously, fuck this module.
    module_disable(array('comment'), TRUE);
    $this->resetAll();

    // Create test user.
    $this->adminUser = $this->drupalCreateUser(
      array(
        'access content',
        'administer content types',
        'administer nodes',
        'bypass node access',
      ));
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tweak the page body display options.
   *
   * @see FieldUIManageDisplayTestCase::testFormatterUI();
   */
  public function testFieldAdminUi() {
    // Edit the bundle field settings.
    // Example class we want to apply directly to an img tag. @see Bootstrap.
    $testClass = 'img-responsive';

    // ImageFieldDisplayTestCase cheats sets formatter settings directly.
    // I want to test the UI also. The hard way.
    // Take examples from
    // FieldUIManageDisplayTestCase::testFormatterUI

    // Display the "Manage display" screen.
    $manage_fields = 'admin/structure/types/manage/' . $this->testBundle;
    $manage_display = $manage_fields . '/display';
    $this->drupalGet($manage_display);
    $this->assertFieldByName('fields[' . $this->testField . '][type]', 'image', 'The expected field is available is selected.');

    // Press the field formatter edit button.
    // Hopefully it's accessible to non-js clients.
    $this->drupalPostAJAX(NULL, array(), $this->testField . '_formatter_settings_edit');
    // The resulting (artificially manipulated) page should now show my
    // field formatter settings. Check if the label is seen.
    $this->assertRaw(t('Element Classes'), 'Field formatter settings for entering Element classes can be found on the manage display form.');

    $edit = array();
    $edit['fields[' . $this->testField . '][settings_edit_form][settings][more_classes]'] = $testClass;
    $this->drupalPostAJAX(NULL, $edit, $this->testField . '_formatter_settings_update');
    $summary = t('Element Classes: @more_classes', array('@more_classes' => $testClass));
    $this->assertRaw($summary, 'Field formatter settings updated.');

    $this->drupalPost(NULL, array(), t('Save'));
    $this->assertRaw(t('Your settings have been saved.'), 'Field formatter settings saved.');

    // Now load content, by adding a node and populating its fields.
    $test_image = current($this->drupalGetTestFiles('image'));
    $nid = $this->uploadNodeImage($test_image, $this->testField, $this->testBundle);
    // I should now be able to look at the node page and see an image with
    // the class attribute I defined.
    $this->assertFieldByXPath('//img[@class="' . $testClass . '"]', NULL, 'The img on this page has the new class attribute applied to it.');
  }

  /**
   * Upload an image to a node.
   *
   * Copied from ImageFieldTestCase::uploadNodeImage();
   *
   * @param $image
   *   A file object representing the image to upload.
   * @param $field_name
   *   Name of the image field the image should be attached to.
   * @param $type
   *   The type of node to create.
   */
  function uploadNodeImage($image, $field_name, $type) {
    $edit = array(
      'title' => $this->randomName(),
    );
    $edit['files[' . $field_name . '_' . LANGUAGE_NONE . '_0]'] = drupal_realpath($image->uri);
    $this->drupalPost('node/add/' . $type, $edit, t('Save'));

    // Retrieve ID of the newly created node from the current URL.
    $matches = array();
    preg_match('/node\/([0-9]+)/', $this->getUrl(), $matches);
    return isset($matches[1]) ? $matches[1] : FALSE;
  }

}